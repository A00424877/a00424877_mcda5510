import java.io.*;

public class SimpleFileReader {
	public  void Read(String args) {//(String[] args) {
		SimpleCsvParser  pr = new SimpleCsvParser();
		// The name of the file to open.
		String fileName = args;//"C:\\Users\\rodol\\Documents\\Saint Mary\\MCDA5510\\Assignment3\\JavaSamples\\temp.txt";

		// This will reference one line at a time
		String line = null;

		try {
			// FileReader reads text files in the default encoding.
			FileReader fileReader = new FileReader(fileName);

			// Always wrap FileReader in BufferedReader.
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				
				//call parser
				
				System.out.println(line);
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
			// Or we could just do this:
			// ex.printStackTrace();
		}
	}
}
