import java.io.File;


public class DirWalker {

    public void walk( String path ) {

        File root = new File( path );
        File[] list = root.listFiles();
        SimpleCsvParser fr = new SimpleCsvParser();
        
        

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
                System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
                System.out.println( "File:" + f.getAbsoluteFile() );
                
                fr.Parse(f.getAbsoluteFile().toString());
            }
        }
    }

    public static void main(String[] args) {
    	DirWalker        fw = new DirWalker();
    	
    	
    	SimpleExceptions ex = new SimpleExceptions();
    	SimpleLogging    lg = new SimpleLogging();
    
    	// Walk trough every folder in the locations with the samples   	
        //fw.walk("C:\\Users\\rodol\\Documents\\Saint Mary\\MCDA5510\\Assignment3\\JavaSamples\\Sample Data" );
        fw.walk("./Sample Data/2017");
        
    }

}