import java.io.FileReader;
import java.lang.Object;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class SimpleCsvParser {

	public void Parse(String args) {
        SimpleLogging sl = new SimpleLogging();
        SimpleExceptions ex = new SimpleExceptions();
      
        
		Reader in;				
				
				// = new FileWriter("./AllCSV.csv",true);
		int CountFail = 0; 
		int CountGood = 0;
		try {
			
			in = new FileReader(args);//("C:\\Users\\rodol\\Documents\\Saint Mary\\MCDA5510\\Assignment3\\JavaSamples\\sampleFile.csv");
			//out = new Writer("");
			BufferedWriter out = new BufferedWriter(new FileWriter("./Output/AllCSV.csv",true));
			BufferedWriter log = new BufferedWriter(new FileWriter("./Output/Log.txt",true));
			final long startTime = System.currentTimeMillis();
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			for (CSVRecord record : records) {
			    
				String id = record.get(0);				
			    String refID = record.get(1);
			    String Street = record.get(2);
			    String City = record.get(3);
			    String Province = record.get(4);
			    String Country = record.get(5);
			    String PCode = record.get(6);
			    String PNum = record.get(7);
			    String Email = record.get(8);
			    
			    if (record.getRecordNumber() != 1)
			    {
			      String Alls = record.get(0)+','+record.get(1)+','+record.get(2)+','+record.get(3)+','+record.get(4)
			      +','+record.get(5)+','+record.get(6)+','+record.get(7)+','+record.get(8);
			      
			      if ((id.length() == 0) || (refID.length() == 0) || (Street.length() == 0) || (City.length() == 0) || (Province.length() == 0)
			    		|| (Country.length() == 0) || (PCode.length() == 0) || (PNum.length() == 0) || (Email.length() == 0)) 
			      {
			    	CountFail ++; // Counts the bad rows
			    	
			      }
			      else
			      {
			    	out.append(Alls);
			    	out.newLine();
			    	CountGood ++;
			    	//put in into the file 
			      }
			    }
			    
			    System.out.println("Data: "+ id+" : "+refID +Street+City+Province+Country+PCode+PNum+Email);
			}		
			
		    out.close();
			//ex.exception(Integer.toString(CountFail));
			//sl.Log(args);
		    final long endTime = System.currentTimeMillis();
		    log.append(args + " Total execution time: " + (endTime - startTime) +" ms");
		    log.newLine();
		    log.append("Rows with errors in this file : " + (CountFail));
		    log.newLine();
		    log.append("Total rows without errors : " + (CountGood));
		    log.newLine();
		    log.close();
		    System.out.println(args + "Total execution time: " + (endTime - startTime) +" ms");
			System.out.println(CountFail);
			
		} catch ( IOException e) {
			e.printStackTrace();
		}

		
		
	}

}
