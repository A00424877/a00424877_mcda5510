import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SimpleExceptions {

	public void exception(String args) {

		FileReader fr = null;
		
		
		
		try {
			File file = new File("C:\\Users\\rodol\\Documents\\Saint Mary\\MCDA5510\\Assignment3\\JavaSamples\\file.txt");
			fr = new FileReader(file);
		
			
			char[] a = args.toCharArray();//new char[50];
			
			fr.read(a); // reads the content to the array
			
			for (char c : a)
				System.out.print(c); // prints the characters one by one
		} catch (IOException e) { 
			e.printStackTrace();
		} finally {
			try {
				fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
