﻿//  File ContactForm
// 
// Assignment #2
// Due Feb 12th
//
// Requirement 1: Expand on this form to display information in database that displays the following fields
// First Name (TextBox) - max lenght 50
// Last Name (TextBox) - max lenght 50
// Street Number (TextBox) - number
// Address (TextBox) - max lenght 50
// City (TextBox) - max lenght 50
// Province (TextBox) - max lenght 50
// Country (TextBox) - Text no validation - Canada
// Postal Code  (TextBox) - Max 7 characters ( 6 plus space)
// Phone Number (TextBox)
// email Address (TextBox)
//
// Database will allow fields that exceed validation requiremernts above
// Validation will be executed as we use the next previous buttons
//
//
// Add Next and Prevous Buttons to navigate through the database ( handle index 0 and max index)
// Display the current primary key of the database in a textbox or label
// Add a Status TextBox and dispaly any validation errors that are encoutered, 
// If multiple errors exist only show one.

// Requirement 2: Expand on the below example to create a import the contents of the CSV file 
// created in Assignment1, read the data into entity classes and save data to database.  
// After import Next and Prev buttons should work.
//
// TODO for Dan - add example of how to save data
//
// Please always try to write clean And readable code
// Here Is a good reference doc http://ricardogeek.com/docs/clean_code.html  
// Submit to Bitbucket under Assignment2

namespace WindowsContactForm
{
    partial class ContactForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.prevButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.textImportFile = new System.Windows.Forms.TextBox();
            this.importLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonImport = new System.Windows.Forms.Button();
            this.txBLastName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbProvince = new System.Windows.Forms.TextBox();
            this.tbCity = new System.Windows.Forms.TextBox();
            this.lbProvince = new System.Windows.Forms.Label();
            this.lbCity = new System.Windows.Forms.Label();
            this.lbStreet = new System.Windows.Forms.Label();
            this.tbCountry = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPostalCode = new System.Windows.Forms.TextBox();
            this.lbPostalCode = new System.Windows.Forms.Label();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.lbPhone = new System.Windows.Forms.Label();
            this.lbEmail = new System.Windows.Forms.Label();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.lbAddress = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lbError = new System.Windows.Forms.Label();
            this.tbELog = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tbStreet = new System.Windows.Forms.MaskedTextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-69, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Primary Key";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(92, 21);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(57, 20);
            this.textBoxID.TabIndex = 1;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(91, 60);
            this.textBoxFirstName.MaxLength = 50;
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(186, 20);
            this.textBoxFirstName.TabIndex = 2;
            this.textBoxFirstName.TextChanged += new System.EventHandler(this.textBoxFirstName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-66, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "First Name";
            // 
            // prevButton
            // 
            this.prevButton.Enabled = false;
            this.prevButton.Location = new System.Drawing.Point(60, 208);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 23);
            this.prevButton.TabIndex = 4;
            this.prevButton.Text = "Previous";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButtonClick);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(167, 208);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 5;
            this.nextButton.Text = "Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButtonClick);
            // 
            // textImportFile
            // 
            this.textImportFile.Location = new System.Drawing.Point(86, 16);
            this.textImportFile.Name = "textImportFile";
            this.textImportFile.Size = new System.Drawing.Size(243, 20);
            this.textImportFile.TabIndex = 6;
            this.textImportFile.TextChanged += new System.EventHandler(this.textImportFile_TextChanged);
            // 
            // importLabel
            // 
            this.importLabel.AutoSize = true;
            this.importLabel.Location = new System.Drawing.Point(3, 19);
            this.importLabel.Name = "importLabel";
            this.importLabel.Size = new System.Drawing.Size(82, 13);
            this.importLabel.TabIndex = 7;
            this.importLabel.Text = "Import CSV File:";
            this.importLabel.Click += new System.EventHandler(this.importLabel_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(335, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(86, 43);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(75, 23);
            this.buttonImport.TabIndex = 9;
            this.buttonImport.Text = "Import";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // txBLastName
            // 
            this.txBLastName.Location = new System.Drawing.Point(357, 60);
            this.txBLastName.MaxLength = 50;
            this.txBLastName.Name = "txBLastName";
            this.txBLastName.Size = new System.Drawing.Size(179, 20);
            this.txBLastName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(293, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Last Name";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // tbProvince
            // 
            this.tbProvince.Location = new System.Drawing.Point(264, 148);
            this.tbProvince.MaxLength = 50;
            this.tbProvince.Name = "tbProvince";
            this.tbProvince.Size = new System.Drawing.Size(100, 20);
            this.tbProvince.TabIndex = 13;
            // 
            // tbCity
            // 
            this.tbCity.Location = new System.Drawing.Point(435, 148);
            this.tbCity.MaxLength = 50;
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(100, 20);
            this.tbCity.TabIndex = 14;
            // 
            // lbProvince
            // 
            this.lbProvince.AutoSize = true;
            this.lbProvince.Location = new System.Drawing.Point(208, 151);
            this.lbProvince.Name = "lbProvince";
            this.lbProvince.Size = new System.Drawing.Size(49, 13);
            this.lbProvince.TabIndex = 15;
            this.lbProvince.Text = "Province";
            // 
            // lbCity
            // 
            this.lbCity.AutoSize = true;
            this.lbCity.Location = new System.Drawing.Point(386, 151);
            this.lbCity.Name = "lbCity";
            this.lbCity.Size = new System.Drawing.Size(24, 13);
            this.lbCity.TabIndex = 16;
            this.lbCity.Text = "City";
            this.lbCity.Click += new System.EventHandler(this.label5_Click);
            // 
            // lbStreet
            // 
            this.lbStreet.AutoSize = true;
            this.lbStreet.Location = new System.Drawing.Point(-66, 96);
            this.lbStreet.Name = "lbStreet";
            this.lbStreet.Size = new System.Drawing.Size(35, 13);
            this.lbStreet.TabIndex = 17;
            this.lbStreet.Text = "Street";
            // 
            // tbCountry
            // 
            this.tbCountry.Location = new System.Drawing.Point(92, 148);
            this.tbCountry.MaxLength = 50;
            this.tbCountry.Name = "tbCountry";
            this.tbCountry.Size = new System.Drawing.Size(100, 20);
            this.tbCountry.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Country";
            // 
            // tbPostalCode
            // 
            this.tbPostalCode.Location = new System.Drawing.Point(357, 89);
            this.tbPostalCode.MaxLength = 7;
            this.tbPostalCode.Name = "tbPostalCode";
            this.tbPostalCode.Size = new System.Drawing.Size(178, 20);
            this.tbPostalCode.TabIndex = 20;
            // 
            // lbPostalCode
            // 
            this.lbPostalCode.AutoSize = true;
            this.lbPostalCode.Location = new System.Drawing.Point(293, 92);
            this.lbPostalCode.Name = "lbPostalCode";
            this.lbPostalCode.Size = new System.Drawing.Size(64, 13);
            this.lbPostalCode.TabIndex = 21;
            this.lbPostalCode.Text = "Postal Code";
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(388, 175);
            this.tbPhone.MaxLength = 150;
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(147, 20);
            this.tbPhone.TabIndex = 22;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(91, 175);
            this.tbEmail.MaxLength = 150;
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(201, 20);
            this.tbEmail.TabIndex = 23;
            // 
            // lbPhone
            // 
            this.lbPhone.AutoSize = true;
            this.lbPhone.Location = new System.Drawing.Point(304, 179);
            this.lbPhone.Name = "lbPhone";
            this.lbPhone.Size = new System.Drawing.Size(78, 13);
            this.lbPhone.TabIndex = 24;
            this.lbPhone.Text = "Phone Number";
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(51, 178);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(32, 13);
            this.lbEmail.TabIndex = 25;
            this.lbEmail.Text = "Email";
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(91, 121);
            this.tbAddress.MaxLength = 50;
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(444, 20);
            this.tbAddress.TabIndex = 26;
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Location = new System.Drawing.Point(41, 121);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(45, 13);
            this.lbAddress.TabIndex = 27;
            this.lbAddress.Text = "Address";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lbError);
            this.panel1.Controls.Add(this.tbELog);
            this.panel1.Location = new System.Drawing.Point(594, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(244, 171);
            this.panel1.TabIndex = 29;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Click on TextBox to acknowledge";
            // 
            // lbError
            // 
            this.lbError.AutoSize = true;
            this.lbError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbError.ForeColor = System.Drawing.Color.Black;
            this.lbError.Location = new System.Drawing.Point(7, 21);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(58, 13);
            this.lbError.TabIndex = 12;
            this.lbError.Text = "Status Log";
            // 
            // tbELog
            // 
            this.tbELog.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbELog.Location = new System.Drawing.Point(9, 39);
            this.tbELog.MaximumSize = new System.Drawing.Size(300, 70);
            this.tbELog.Multiline = true;
            this.tbELog.Name = "tbELog";
            this.tbELog.Size = new System.Drawing.Size(225, 70);
            this.tbELog.TabIndex = 0;
            this.tbELog.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbELog_MouseClick);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tbStreet);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.nextButton);
            this.panel2.Controls.Add(this.textBoxFirstName);
            this.panel2.Controls.Add(this.lbAddress);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.tbAddress);
            this.panel2.Controls.Add(this.lbEmail);
            this.panel2.Controls.Add(this.textBoxID);
            this.panel2.Controls.Add(this.lbPhone);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tbEmail);
            this.panel2.Controls.Add(this.txBLastName);
            this.panel2.Controls.Add(this.tbPhone);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.tbCountry);
            this.panel2.Controls.Add(this.lbPostalCode);
            this.panel2.Controls.Add(this.lbCity);
            this.panel2.Controls.Add(this.lbStreet);
            this.panel2.Controls.Add(this.lbProvince);
            this.panel2.Controls.Add(this.tbPostalCode);
            this.panel2.Controls.Add(this.tbCity);
            this.panel2.Controls.Add(this.prevButton);
            this.panel2.Controls.Add(this.tbProvince);
            this.panel2.Location = new System.Drawing.Point(16, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(572, 254);
            this.panel2.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "First Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Street";
            this.label5.Click += new System.EventHandler(this.label5_Click_1);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textImportFile);
            this.panel3.Controls.Add(this.importLabel);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.buttonImport);
            this.panel3.Location = new System.Drawing.Point(16, 272);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(572, 81);
            this.panel3.TabIndex = 31;
            // 
            // tbStreet
            // 
            this.tbStreet.Location = new System.Drawing.Point(92, 89);
            this.tbStreet.Mask = "99999";
            this.tbStreet.Name = "tbStreet";
            this.tbStreet.Size = new System.Drawing.Size(185, 20);
            this.tbStreet.TabIndex = 31;
            this.tbStreet.ValidatingType = typeof(int);
            // 
            // ContactForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 357);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ContactForm";
            this.Text = "Contact Form";
            this.Load += new System.EventHandler(this.ContactForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.TextBox textImportFile;
        private System.Windows.Forms.Label importLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.TextBox txBLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbProvince;
        private System.Windows.Forms.TextBox tbCity;
        private System.Windows.Forms.Label lbProvince;
        private System.Windows.Forms.Label lbCity;
        private System.Windows.Forms.Label lbStreet;
        private System.Windows.Forms.TextBox tbCountry;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPostalCode;
        private System.Windows.Forms.Label lbPostalCode;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Label lbAddress;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbError;
        private System.Windows.Forms.TextBox tbELog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox tbStreet;
    }
}

