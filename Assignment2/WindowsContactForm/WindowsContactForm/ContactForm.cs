﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Windows.Forms;
// Project -> Add references
using Microsoft.VisualBasic.FileIO;

namespace WindowsContactForm
{
    public partial class ContactForm : Form
    {
        Dictionary<int, Customer> allData = new Dictionary<int, Customer>();
        int currentIndex = 0;
        int maxIndex = 0;

        public ContactForm()
        {
           
            InitializeComponent();
        }

        private void prevButtonClick(object sender, EventArgs e)
        {
            if (currentIndex > 0) 
            {
                currentIndex = currentIndex - 1;
                UpdateData(currentIndex);
            }
        }

        private void nextButtonClick(object sender, EventArgs e)
        {
            currentIndex = currentIndex + 1;
            UpdateData(currentIndex);
        }

        private void ContactForm_Load(object sender, EventArgs e)
        {
            allData.Clear();
            this.prevButton.Enabled = false; // this button must be disable when the app runs the first time


            using (CustomerDataModel db = new CustomerDataModel())
            {
                DbSet<Customer> customers = db.Customers;

                foreach (Customer customer in customers)
                {
                    allData.Add(maxIndex, customer);
                    maxIndex++;
                }
            }
            UpdateData(currentIndex);


        }

        private void UpdateData(int index)
        {
            Boolean flag = false;
            string vfield;
            Customer personData = null;
            try
            {
                if (allData.TryGetValue(index, out personData))
                {
                    flag = CheckLenght(personData.FirstName, this.textBoxFirstName.MaxLength);
                    if (flag == false)
                    {
                        this.textBoxFirstName.Text = personData.FirstName;
                    }
                    else
                    {
                        this.textBoxFirstName.Text = null;
                        vfield = this.textBoxFirstName.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.ID.ToString(), this.textBoxID.MaxLength);
                    if (flag == false)
                    {
                        this.textBoxID.Text = personData.ID.ToString();
                    }
                    else
                    {
                        this.textBoxID.Text = null;
                        vfield = this.textBoxID.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.LastName.ToString(), this.txBLastName.MaxLength);
                    if (flag == false)
                    {
                        this.txBLastName.Text = personData.LastName.ToString();
                    }
                    else
                    {
                        this.txBLastName.Text = null;
                        this.tbPhone.Text = null;
                        vfield = this.txBLastName.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.Address.ToString(), this.tbAddress.MaxLength);
                    if (flag == false)
                    {
                        this.tbAddress.Text = personData.Address.ToString();
                    }
                    else
                    {
                        this.tbAddress.Text = null;
                        vfield = this.tbAddress.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.City.ToString(), this.tbCity.MaxLength);
                    if (flag == false)
                    {
                        this.tbCity.Text = personData.City.ToString();
                    }
                    else
                    {
                        this.tbCity.Text = null;
                        vfield = this.tbCity.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.Country.ToString(), this.tbCountry.MaxLength);
                    if (flag == false)
                    {
                        this.tbCountry.Text = personData.Country.ToString();
                    }
                    else
                    {
                        this.tbCountry.Text = null;
                        vfield = this.tbCountry.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.PostalCode.ToString(), this.tbPostalCode.MaxLength);
                    if (flag == false)
                    {
                        this.tbPostalCode.Text = personData.PostalCode.ToString();
                    }
                    else
                    {
                        this.tbPostalCode.Text = null;
                        vfield = this.tbPostalCode.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.Province.ToString(), this.tbProvince.MaxLength);
                    if (flag == false)
                    {
                        this.tbProvince.Text = personData.Province.ToString();
                    }
                    else
                    {
                        this.tbProvince.Text = null;
                        vfield = this.tbProvince.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.StreetNumber.ToString(), this.tbStreet.MaxLength);
                    if (flag == false)
                    {
                        this.tbStreet.Text = personData.StreetNumber.ToString();
                    }
                    else
                    {
                        this.tbStreet.Text = null;
                        vfield = this.tbStreet.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.Email.ToString(), this.tbEmail.MaxLength);
                    if (flag == false)
                    {
                        this.tbEmail.Text = personData.Email.ToString();
                    }
                    else
                    {
                        this.tbEmail.Text = null;
                        vfield = this.tbEmail.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }

                    flag = CheckLenght(personData.PhoneNumber.ToString(), this.tbPhone.MaxLength);
                    if (flag == false)
                    {
                        this.tbPhone.Text = personData.PhoneNumber.ToString();
                    }
                    else
                    {
                        this.tbPhone.Text = null;
                        vfield = this.tbPhone.Name;
                        FieldLengthError(vfield);
                        flag = false;
                    }
                }



                if (currentIndex == maxIndex - 1)
                {
                    this.nextButton.Enabled = false;
                }
                else if (currentIndex == 0)
                {
                    this.prevButton.Enabled = false;
                }
                else if (this.nextButton.Enabled == false)
                {
                    this.nextButton.Enabled = true;
                }
                else if (this.prevButton.Enabled == false)
                {
                    this.prevButton.Enabled = true;
                }

            }
            catch (Exception excp) // Exception handler if some error uploading the data
            {
                this.tbELog.Text = "Exception retrieving data: {0}" + excp;

                this.lbError.ForeColor = System.Drawing.Color.Red;// If there is a exception, it will change the color of the label
                                                                  // and include the message in the textbox
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                textImportFile.Text = openFileDialog1.FileName;
            }
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            //String file = @"C: \Users\dpenny\Documents\Source\Repos\MSCDA_5510_Samples\Assignment2\WindowsContactForm\WindowsContactForm\names.csv";
            String file = textImportFile.Text;

            try
            {

                using (TextFieldParser parser = new TextFieldParser(file))
                {
                    parser.Delimiters = new string[] { "," };
                    while (true)
                    {
                        try
                        {
                            string[] parts = parser.ReadFields();
                            if (parts == null)
                            {
                                break;
                            }
                            using (CustomerDataModel db = new CustomerDataModel())
                            {

                                DbSet<Customer> customers = db.Customers;
                                Customer cust = new Customer();
                                cust.FirstName = parts[0];
                                cust.LastName = parts[1];
                                cust.StreetNumber = Int32.Parse(parts[2]);
                                cust.Address = parts[3];
                                cust.City = parts[4];
                                cust.Province = parts[5];
                                cust.Country = parts[6];
                                cust.PostalCode = parts[7];
                                cust.PhoneNumber = parts[8];
                                cust.Email = parts[9];
                                // this saves it in the DB to be saved when Save is called
                                customers.Add(cust);
                                db.SaveChanges();
                                allData.Add(maxIndex, cust);
                                maxIndex++;
                            }
                        }

                        catch (Exception excp) // Exception handler if some error uploading the data
                        {
                            this.tbELog.Text = "Exception loading data source: {0}" + excp;

                            this.lbError.ForeColor = System.Drawing.Color.Red;// If there is a exception, it will change the color of the label
                                                                              // and include the message in the textbox


                        }
                    }
                }
                UpdateData(currentIndex);
                this.textImportFile.Text = null; // Deletes the name of the file if succeed upload the data
            }
            catch (Exception excp)// exception handler  if the file name is empty
            {
                this.tbELog.Text = "Exception loading data source: {0}" + excp;

                this.lbError.ForeColor = System.Drawing.Color.Red; // If there is a exception, it will change the color of the label
                                                                   // and include the message in the textbox
            }


        }

        public Boolean CheckLenght(string v_newdata, int v_fieldlen)
        {
            Boolean vcheck;

            v_newdata = v_newdata.Trim();
            if (v_newdata.Length > v_fieldlen)
            {
                vcheck = true;
            }
            else
            {
                vcheck = false;
            }

            return vcheck;
        }


        public void FieldLengthError(string fieldn)
        {
            this.tbELog.Text = "Error in length in field: {0}" + fieldn;

            this.lbError.ForeColor = System.Drawing.Color.Red;

        }

        private void textImportFile_TextChanged(object sender, EventArgs e)
        {

        }

        private void importLabel_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBoxFirstName_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click_1(object sender, EventArgs e)
        {

        }

        private void tbELog_MouseClick(object sender, MouseEventArgs e)
        {
            this.lbError.ForeColor = System.Drawing.Color.Black;
            this.tbELog.Text       = null;
        }
    }
    }
