USE [A00424877]
GO

/****** Object:  Table [dbo].[customer]    Script Date: 11/2/2018 22:52:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nchar](500) NULL,
	[LastName] [nchar](500) NULL,
	[StreetNumber] [int] NULL,
	[Address] [nchar](500) NULL,
	[City] [varchar](500) NULL,
	[Province] [varchar](500) NULL,
	[Country] [varchar](500) NULL,
	[PostalCode] [nchar](70) NULL,
	[PhoneNumber] [nchar](400) NULL,
	[Email] [nchar](800) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO