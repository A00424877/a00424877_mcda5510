# A00424877_MCDA5510

First Assignmet course MCDA5510

My name is Rodolfo Garcia, I'm from Costa Rica and I'm really excited to be in Canada studying. I'm married since 2005 and I have 2 handsome boys, one is 9 years old and the younger one is 5. Also I have 14 years of experience in software development
getting the opportunity to share with international companies represented in Costa Rica, I was working in several roles like developer, business analyst, scrum master and project manager.

I would like this topics to be review in the course:
1. Differences between processes on different projects, for example in a mobile application and web application. Also when an interface is needed, I mean when projects are not always involved a final user. How it will be managed? How the Life cycle change?
2. New tendencies on development of projects for Artificial Intelligence, what languages are being used?
3. How the companies manage the risk in projects larger than 1 million dollars in budget and larger than 2 years in time?

I'm familiar with .Net, java, C# as well but because of my specialization in Costa Rica I haven't worked with that technologies since 2003. I also know C++ and Oracle Forms 6i,10g,11g and 12c.

I really want to know in this course if there is some differences about how the projects are planned and managed depending on different technologies and results, to understand if the market has changed the way of how we create software. New tendencies and how the software development is changing to fit the needs of the current markets.

Thank you